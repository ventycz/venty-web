interface StateData {
    track: {
        artist: string
        name: string
        length: number
        progress: number
        link: string
        progress_real?: number
    }
}

export default interface State {
    updated: Date
    current: StateData
}
