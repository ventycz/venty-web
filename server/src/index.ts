import fsPromise from 'fs/promises';
import SpotifyWebApi from 'spotify-web-api-node';
import uWS from 'uWebSockets.js';
import State from './state';

const { clientId, clientSecret } = require('../config/secrets.json');
const { refresh: refreshToken } = require('../config/tokens.json');
const { redirectUri, serverPort } = require('../config/app.json');

if (!refreshToken?.length) {
    console.error('No refresh token, run scripts/auth.js first!');
    process.exit(1);
}

function setupServer() {
    return uWS.App().ws('/ws', {
        compression: uWS.SHARED_COMPRESSOR,
        maxPayloadLength: 16 * 1024 * 1024,
        idleTimeout: 12,

        open: (ws) => {
            ws.subscribe('spotify');
        }
    }).any('/*', (res, req) => {
        res.end('Nothing to see here!')
    }).listen(serverPort, (token) => {
        if (token) {
            console.log('Listening to port ' + serverPort);
        } else {
            console.log('Failed to listen to port ' + serverPort);
            process.exit(1);
        }
    });
}

async function sleep(secs: number) {
    return new Promise((resolve) => setTimeout(resolve, secs * 1000));
}

async function refresh(api: SpotifyWebApi) {
    const { body: resp } = await api.refreshAccessToken();
    api.setAccessToken(resp.access_token);

    await fsPromise.writeFile(`${__dirname}/../config/tokens.json`, JSON.stringify({
        access: resp.access_token,
        refresh: api.getRefreshToken()
    }, null, 4));
}

async function run(api: SpotifyWebApi): Promise<State> {
    const { body: resp } = await api.getMyCurrentPlaybackState();

    const state: State = {
        updated: new Date(),
        current: null
    };

    if ('track' !== resp.currently_playing_type) {
        return state;
    }

    if (!resp.is_playing || !resp.item) {
        return state;
    }

    //@ts-ignore
    const artist = resp.item.artists[0].name;

    state.current = {
        track: {
            artist: artist,
            name: resp.item.name,
            length: resp.item.duration_ms,
            progress: resp.progress_ms,
            link: resp.item.external_urls.spotify
        }
    };

    return state;
}

function sync(server: uWS.TemplatedApp, state: State) {
    if (!state.current) {
        server.publish('spotify', JSON.stringify({
            track: null
        }));

        return;
    }

    const sinceUpdateMs = (new Date).getTime() - state.updated.getTime();

    const track: State['current']['track'] = state.current.track;
    track.progress_real = Math.min(track.progress + sinceUpdateMs, track.length);

    server.publish('spotify', JSON.stringify({
        track
    }));
}

(async () => {
    const api = new SpotifyWebApi({
        clientId,
        clientSecret,
        redirectUri
    });

    api.setRefreshToken(refreshToken);
    await refresh(api);

    let lastState: State = {
        updated: null,
        current: null
    };
    const server = setupServer();

    setInterval(() => {
        if (!lastState.updated) {
            return;
        }

        sync(server, lastState);
    }, 1000);

    while(true) {
        try {
            lastState = await run(api);
        } catch (e) {
            console.warn(e);
            await refresh(api);
            continue;
        }

        await sleep(5);
    }
})();
