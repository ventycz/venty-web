#!/usr/bin/node
//@ts-check
const { normalize } = require('path');

/**
 * @param {string} dir
 */
function buildService(dir) {
    dir = dir.replace(/\/$/, '');

    return (`
        [Unit]
        Description=Venty Server
        After=network.target

        [Service]
        Environment=NODE_ENV=production
        Type=simple
        ExecStart=/usr/bin/node ${dir}/index.js
        WorkingDirectory=${dir}
        Restart=on-failure
        StandardOutput=journal
        StandardError=journal

        [Install]
        WantedBy=multi-user.target
    `).trim().replace(/^ {8}/gm, '');
}

let dir = normalize(`${__dirname}/../dist`);
const serviceContents = buildService(dir);

console.log(serviceContents);
