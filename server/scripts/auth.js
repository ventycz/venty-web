#!/usr/bin/node
//@ts-check
const SpotifyWebApi = require('spotify-web-api-node');
const fsPromise = require('fs/promises');
const prompts = require('prompts');
const { clientId, clientSecret } = require('../config/secrets.json');
const { scopes, redirectUri } = require('../config/app.json');

(async () => {
    const api = new SpotifyWebApi({
        clientId,
        clientSecret,
        redirectUri
    });

    const url = api.createAuthorizeURL(scopes, 'venty-state');
    console.log(url);

    const response = await prompts({
        type: 'text',
        name: 'code',
        message: 'Code:',
        validate: code => code && code.trim().length ? true : 'FFS'
    });

    const { body: resp } = await api.authorizationCodeGrant(response.code);
    await fsPromise.writeFile(`${__dirname}/../config/tokens.json`, JSON.stringify({
        access: resp.access_token,
        refresh: resp.refresh_token
    }, null, 4));
})();

