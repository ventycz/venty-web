<?php
    function getVersion($file) {
        $path = realpath(__DIR__ . '/' . $file);

        // Get last modification timestamp of the file
        $lastModif = filemtime($path);

        // Generate version
        $ver = md5($lastModif);

        // Get only first 8 chars from version
        return substr($ver, 0, 8);
    }

    $vrs = [
        'app.js' => getVersion('dist/app.js'),
        'app.css' => getVersion('dist/app.css')
    ];

    $html = file_get_contents(__DIR__ . '/index.html');
    $html = preg_replace('~^.+DEV-ONLY.+$~m', '', $html);

    // Append version tags to files, that need it
    print preg_replace_callback('~/(dist/app\.(?:js|css))~', function($m) {
        $version = getVersion($m[1]);
        return "$m[0]?v=$version";
    }, $html);
    exit;
?>
