#!/usr/bin/node

//@ts-check
import { resolve } from 'node:path';

import * as esbuild from 'esbuild';
import sveltePlugin from 'esbuild-svelte';

const args = process.argv.slice(1);

/**
 * @type {Record<string, boolean>}
 */
const opts = args.reduce((obj, item) => {
    const m = /^--([a-z]+)$/.exec(item);
    if (!m) return obj;

    obj[m[1]] = true;
    return obj;
}, {});

const rootDir = resolve(import.meta.dirname, '../');

let ctx = await esbuild.context({
    entryPoints: [`${rootDir}/src/app.js`],
    target: [
        'safari12'
    ],
    bundle: true,
    sourcemap: true,
    format: 'iife',
    outdir: `${rootDir}/public/dist/`,
    plugins: [sveltePlugin()],
    mainFields: ["svelte", "browser", "module", "main"],
    conditions: ["svelte", "browser"],
    external: ['*.png'],
});

if (opts.watch) {

    await ctx.watch();

    let { host, port } = await ctx.serve({
        host: '127.0.0.1',
        port: 8124,
        servedir: `${rootDir}/public/`,
        fallback: `${rootDir}/public/index.html`,
    });

    console.log(`Listening on http://${host}:${port}`);
} else {

    await ctx.rebuild();
    await ctx.dispose();
}
