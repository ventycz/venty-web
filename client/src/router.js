import Navigo from 'navigo';
import Pages from './pages';
import state from './state';

let pageIndexes = {};
let i = 0;
for (let page of Pages) {
    pageIndexes[page.key] = i;
    i++;
}

class Router {
    constructor() {
        // Yeah, the root building is kinda awful, but it does not work without it ...
        this.nav = new Navigo(`${location.protocol}//${location.host}`, false);

        // Register not found handler
        this.nav.notFound(() => {
            state.update((cur) => {
                cur.prevPage = cur.page;
                cur.page = 'not-found';
                cur.transition = null;

                return cur;
            });
        });
    }

    addRoute(path, key, params, additional) {
        if ((typeof path !== 'string' && !(path instanceof RegExp)) || !key) return false;

        let routeHandler = (function(path, key, params, additional) {
            return (q) => {
                let props = typeof additional === 'object' ? additional : {};
                if (Array.isArray(params)) {
                    for (let param of params) {
                        if (!q || !q.hasOwnProperty(param)) continue;

                        props[param] = q[param];
                    }
                }

                state.update((cur) => {
                    cur.prevPage = cur.page;
                    cur.page = key;

                    if (cur.prevPage !== null) {
                        let iPrev = pageIndexes[cur.prevPage];
                        let iNext = pageIndexes[cur.page];

                        if (iPrev !== iNext) {
                            cur.transition = iPrev < iNext ? 60 : 61;
                        }
                    }

                    return cur;
                });
            }
        })(path, key, params, additional);

        if (path) {
            this.nav.on(path, routeHandler);
        } else {
            this.nav.on(routeHandler);
        }

        return true;
    }

    addRedirect(path, cb) {
        let routeHandler = (function(path, cb) {
            return (...args) => {
                let newLoc = cb(...args);

                if (newLoc) location.href = newLoc;
            }
        })(path, cb);

        this.nav.on(path, routeHandler);

        return true;
    }

    resolve() {
        return this.nav.resolve();
    }
}

export default new Router();
