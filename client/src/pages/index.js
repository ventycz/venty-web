import About from './about.svelte';
import Workstation from './workstation.svelte';
import Music from './music.svelte';
import YouTube from './youtube.svelte';

export default [
    {
        key: 'about',
        menuTitle: 'About me',
        cmp: About,
        icon: 'user',
        path: '/'
    },
    {
        key: 'workstation',
        title: 'Workstation',
        cmp: Workstation,
        icon: 'rocket',
        path: '/workstation'
    },
    {
        key: 'music',
        title: 'Music',
        cmp: Music,
        icon: 'music-note',
        path: '/music'
    },
    {
        key: 'yt',
        title: 'YouTube - Favorite Channels',
        cmp: YouTube,
        icon: 'camera-video',
        path: '/yt'
    }
];
