export default (selectedAnimNumber) => {
    let inClass, outClass;

    switch(selectedAnimNumber) {
        case 1:
            inClass = 'animated-section-moveFromRight';
            outClass = 'animated-section-moveToLeft';
            break;
        case 2:
            inClass = 'animated-section-moveFromLeft';
            outClass = 'animated-section-moveToRight';
            break;
        case 3:
            inClass = 'animated-section-moveFromBottom';
            outClass = 'animated-section-moveToTop';
            break;
        case 4:
            inClass = 'animated-section-moveFromTop';
            outClass = 'animated-section-moveToBottom';
            break;
        case 5:
            inClass = 'animated-section-moveFromRight animated-section-ontop';
            outClass = 'animated-section-fade';
            break;
        case 6:
            inClass = 'animated-section-moveFromLeft animated-section-ontop';
            outClass = 'animated-section-fade';
            break;
        case 7:
            inClass = 'animated-section-moveFromBottom animated-section-ontop';
            outClass = 'animated-section-fade';
            break;
        case 8:
            inClass = 'animated-section-moveFromTop animated-section-ontop';
            outClass = 'animated-section-fade';
            break;
        case 9:
            inClass = 'animated-section-moveFromRightFade';
            outClass = 'animated-section-moveToLeftFade';
            break;
        case 10:
            inClass = 'animated-section-moveFromLeftFade';
            outClass = 'animated-section-moveToRightFade';
            break;
        case 11:
            inClass = 'animated-section-moveFromBottomFade';
            outClass = 'animated-section-moveToTopFade';
            break;
        case 12:
            inClass = 'animated-section-moveFromTopFade';
            outClass = 'animated-section-moveToBottomFade';
            break;
        case 13:
            inClass = 'animated-section-moveFromRight';
            outClass = 'animated-section-moveToLeftEasing animated-section-ontop';
            break;
        case 14:
            inClass = 'animated-section-moveFromLeft';
            outClass = 'animated-section-moveToRightEasing animated-section-ontop';
            break;
        case 15:
            inClass = 'animated-section-moveFromBottom';
            outClass = 'animated-section-moveToTopEasing animated-section-ontop';
            break;
        case 16:
            inClass = 'animated-section-moveFromTop';
            outClass = 'animated-section-moveToBottomEasing animated-section-ontop';
            break;
        case 17:
            inClass = 'animated-section-moveFromRight animated-section-ontop';
            outClass = 'animated-section-scaleDown';
            break;
        case 18:
            inClass = 'animated-section-moveFromLeft animated-section-ontop';
            outClass = 'animated-section-scaleDown';
            break;
        case 19:
            inClass = 'animated-section-moveFromBottom animated-section-ontop';
            outClass = 'animated-section-scaleDown';
            break;
        case 20:
            inClass = 'animated-section-moveFromTop animated-section-ontop';
            outClass = 'animated-section-scaleDown';
            break;
        case 21:
            inClass = 'animated-section-scaleUpDown animated-section-delay300';
            outClass = 'animated-section-scaleDown';
            break;
        case 22:
            inClass = 'animated-section-scaleUp animated-section-delay300';
            outClass = 'animated-section-scaleDownUp';
            break;
        case 23:
            inClass = 'animated-section-scaleUp';
            outClass = 'animated-section-moveToLeft animated-section-ontop';
            break;
        case 24:
            inClass = 'animated-section-scaleUp';
            outClass = 'animated-section-moveToRight animated-section-ontop';
            break;
        case 25:
            inClass = 'animated-section-scaleUp';
            outClass = 'animated-section-moveToTop animated-section-ontop';
            break;
        case 26:
            inClass = 'animated-section-scaleUp';
            outClass = 'animated-section-moveToBottom animated-section-ontop';
            break;
        case 27:
            inClass = 'animated-section-scaleUpCenter animated-section-delay400';
            outClass = 'animated-section-scaleDownCenter';
            break;
        case 28:
            inClass = 'animated-section-moveFromRight animated-section-delay200 animated-section-ontop';
            outClass = 'animated-section-rotateRightSideFirst';
            break;
        case 29:
            inClass = 'animated-section-moveFromLeft animated-section-delay200 animated-section-ontop';
            outClass = 'animated-section-rotateLeftSideFirst';
            break;
        case 30:
            inClass = 'animated-section-moveFromTop animated-section-delay200 animated-section-ontop';
            outClass = 'animated-section-rotateTopSideFirst';
            break;
        case 31:
            inClass = 'animated-section-moveFromBottom animated-section-delay200 animated-section-ontop';
            outClass = 'animated-section-rotateBottomSideFirst';
            break;
        case 32:
            inClass = 'animated-section-flipInLeft animated-section-delay500';
            outClass = 'animated-section-flipOutRight';
            break;
        case 33:
            inClass = 'animated-section-flipInRight animated-section-delay500';
            outClass = 'animated-section-flipOutLeft';
            break;
        case 34:
            inClass = 'animated-section-flipInBottom animated-section-delay500';
            outClass = 'animated-section-flipOutTop';
            break;
        case 35:
            inClass = 'animated-section-flipInTop animated-section-delay500';
            outClass = 'animated-section-flipOutBottom';
            break;
        case 36:
            inClass = 'animated-section-scaleUp';
            outClass = 'animated-section-rotateFall animated-section-ontop';
            break;
        case 37:
            inClass = 'animated-section-rotateInNewspaper animated-section-delay500';
            outClass = 'animated-section-rotateOutNewspaper';
            break;
        case 38:
            inClass = 'animated-section-moveFromRight';
            outClass = 'animated-section-rotatePushLeft';
            break;
        case 39:
            inClass = 'animated-section-moveFromLeft';
            outClass = 'animated-section-rotatePushRight';
            break;
        case 40:
            inClass = 'animated-section-moveFromBottom';
            outClass = 'animated-section-rotatePushTop';
            break;
        case 41:
            inClass = 'animated-section-moveFromTop';
            outClass = 'animated-section-rotatePushBottom';
            break;
        case 42:
            inClass = 'animated-section-rotatePullRight animated-section-delay180';
            outClass = 'animated-section-rotatePushLeft';
            break;
        case 43:
            inClass = 'animated-section-rotatePullLeft animated-section-delay180';
            outClass = 'animated-section-rotatePushRight';
            break;
        case 44:
            inClass = 'animated-section-rotatePullBottom animated-section-delay180';
            outClass = 'animated-section-rotatePushTop';
            break;
        case 45:
            inClass = 'animated-section-rotatePullTop animated-section-delay180';
            outClass = 'animated-section-rotatePushBottom';
            break;
        case 46:
            inClass = 'animated-section-moveFromRightFade';
            outClass = 'animated-section-rotateFoldLeft';
            break;
        case 47:
            inClass = 'animated-section-moveFromLeftFade';
            outClass = 'animated-section-rotateFoldRight';
            break;
        case 48:
            inClass = 'animated-section-moveFromBottomFade';
            outClass = 'animated-section-rotateFoldTop';
            break;
        case 49:
            inClass = 'animated-section-moveFromTopFade';
            outClass = 'animated-section-rotateFoldBottom';
            break;
        case 50:
            inClass = 'animated-section-rotateUnfoldLeft';
            outClass = 'animated-section-moveToRightFade';
            break;
        case 51:
            inClass = 'animated-section-rotateUnfoldRight';
            outClass = 'animated-section-moveToLeftFade';
            break;
        case 52:
            inClass = 'animated-section-rotateUnfoldTop';
            outClass = 'animated-section-moveToBottomFade';
            break;
        case 53:
            inClass = 'animated-section-rotateUnfoldBottom';
            outClass = 'animated-section-moveToTopFade';
            break;
        case 54:
            inClass = 'animated-section-rotateRoomLeftIn';
            outClass = 'animated-section-rotateRoomLeftOut animated-section-ontop';
            break;
        case 55:
            inClass = 'animated-section-rotateRoomRightIn';
            outClass = 'animated-section-rotateRoomRightOut animated-section-ontop';
            break;
        case 56:
            inClass = 'animated-section-rotateRoomTopIn';
            outClass = 'animated-section-rotateRoomTopOut animated-section-ontop';
            break;
        case 57:
            inClass = 'animated-section-rotateRoomBottomIn';
            outClass = 'animated-section-rotateRoomBottomOut animated-section-ontop';
            break;
        case 58:
            inClass = 'animated-section-rotateCubeLeftIn';
            outClass = 'animated-section-rotateCubeLeftOut animated-section-ontop';
            break;
        case 59:
            inClass = 'animated-section-rotateCubeRightIn';
            outClass = 'animated-section-rotateCubeRightOut animated-section-ontop';
            break;
        case 60:
            inClass = 'animated-section-rotateCubeTopIn';
            outClass = 'animated-section-rotateCubeTopOut animated-section-ontop';
            break;
        case 61:
            inClass = 'animated-section-rotateCubeBottomIn';
            outClass = 'animated-section-rotateCubeBottomOut animated-section-ontop';
            break;
        case 62:
            inClass = 'animated-section-rotateCarouselLeftIn';
            outClass = 'animated-section-rotateCarouselLeftOut animated-section-ontop';
            break;
        case 63:
            inClass = 'animated-section-rotateCarouselRightIn';
            outClass = 'animated-section-rotateCarouselRightOut animated-section-ontop';
            break;
        case 64:
            inClass = 'animated-section-rotateCarouselTopIn';
            outClass = 'animated-section-rotateCarouselTopOut animated-section-ontop';
            break;
        case 65:
            inClass = 'animated-section-rotateCarouselBottomIn';
            outClass = 'animated-section-rotateCarouselBottomOut animated-section-ontop';
            break;
        case 66:
            inClass = 'animated-section-rotateSidesIn animated-section-delay200';
            outClass = 'animated-section-rotateSidesOut';
            break;
        case 67:
            inClass = 'animated-section-rotateSlideIn';
            outClass = 'animated-section-rotateSlideOut';
            break;
    }

    return {
        inClass,
        outClass
    };
};