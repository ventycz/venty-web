import App from './App.svelte';
import Pages from './pages';
import router from './router';
import state from './state';
import { mount } from "svelte";

for (let page of Pages) {
    router.addRoute(page.path, page.key);
}

router.resolve();

const app = mount(App, {
    target: document.body
});

const ws = new WebSocket('wss://venty.cz/ws');
ws.onopen = () => {
    console.log('Connected to WS!');
};
ws.onmessage = ({ data }) => {
    const event = JSON.parse(data);
    state.setTrack(event.track);
};
ws.onclose = () => {
    state.setTrack(null);
};

export default app;