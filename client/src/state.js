import { writable, get } from 'svelte/store';

const store = writable({
    page: null,
    prevPage: null,
    transition: null,
    track: null
});

export default {
    subscribe: store.subscribe,
    get: () => get(store),
    set: store.set,
    update: store.update,

    setTrack: (track) => {
        store.update(state => {
            state.track = track;
            return state;
        });
    }
};